﻿( () => {
const replyRender = () => {
	const moveCaletLast = function(targetElement) {
		const selection = window.getSelection();		
		const range = document.createRange();
		var offset = 0;
		var childNodes = targetElement.childNodes;
		for(var i = 0;i < childNodes.length;i++){
			offset++;
		}
		
		range.setStart(targetElement, offset);
		range.setEnd(targetElement, offset++);
		range.collapse(true);
	
		targetElement.focus();
		selection.removeAllRanges();
		selection.addRange(range);
		
		
		
		setTimeout(() => {
		  targetElement.focus();
		}, 500);
	}
	
	const elements = document.getElementsByClassName('eWw5ab');

	for( var i = 0; i < elements.length; i++  ){
		const replyElements = elements[i].getElementsByClassName('google-chat-reply');
		if(!replyElements.length){
			const element = document.createElement('div');
		
			element.classList.add('google-chat-reply');
			element.classList.add('mUbCce');
			element.textContent = '返';
			element.style.height = '20px';
			element.style.width = '20px';
			element.style.marginLeft = '4px';
			element.style.marginRight = '4px';
			element.style.fontWeight = 'bold';
			element.style.color = "gray";

			element.addEventListener('click', function(){
				/*
				
				
				//スレッド形式かどうかを判定して、返信メッセージのテキストエリアを判定
				currentRoom = document.body.querySelector('[role="main"]');
				if (currentRoom === undefined || currentRoom === null) {
					console.log('Room element not found.');
					return null;
				}
				const threadHeaders = currentRoom.querySelectorAll('[role="heading"][aria-label]');
				threadHeadersArray = Array.from(threadHeaders);
				if (threadHeadersArray.length == 0) {
					baseElement = document;
				} else {
					baseElement = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
				}
				*/
				
				baseElement = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
				
				const textAreaElements = [];
				textAreaElements[0] = baseElement.querySelector('[jsname="yrriRe"]');
				if(!textAreaElements[0]){
					baseElement = document;
					textAreaElements[0] = baseElement.querySelector('[jsname="yrriRe"]');
				}
				
				
				
				
				let textAreaIndex = 0;

				//user_id取得
				messageWrapElement = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
				
				
				//email取得
				iconElements = messageWrapElement.getElementsByClassName('Fh7ONe YQsadf');
				const user_email = iconElements[0].dataset.hovercardId;
				//user_name取得
				const user_name = iconElements[0].dataset.name;
				//user_id取得
				let user_id = iconElements[0].dataset.memberId;
				user_id = user_id.replace(/[^0-9]+/,'');
				
				//replyElement作成
				const replyElement = document.createElement('span');
				replyElement.textContent = '@' + user_name;
				replyElement.classList.add('PMAryf');
				replyElement.dataset.userId = user_id;
				replyElement.dataset.userType = 0;
				replyElement.dataset.userMentionType = 3;
				replyElement.dataset.userEmail = user_email;
				replyElement.dataset.displayName= "@" + user_name;
				replyElement.setAttribute('contenteditable', 'false');
				
				
				
				let message = this.parentElement.parentElement.parentElement.parentElement.firstChild.firstChild.textContent;
				
				textAreaElements[textAreaIndex].click();
				const selectedText = window.getSelection().toString();
				//console.log(selectedText);
				if(selectedText){
					message = selectedText;
				}
				
				//メッセージを加工してテキストエリアへ挿入				
				textAreaElements[textAreaIndex].textContent = '``` ' + message.replace(/`+/g, '') + ' ```' + "\r\n ";// + '>';
				
				//メンションを追加
				textAreaElements[textAreaIndex].appendChild(replyElement);
				
				//敬称追加＆改行を末尾へ追加
				//var textNode = document.createTextNode("さん\r\n ");
				var textNode = document.createTextNode("\r\n ");
				textAreaElements[textAreaIndex].appendChild(textNode);
				//キャレットを末尾へ移動
				moveCaletLast(textAreaElements[textAreaIndex]);
			});

			if(!elements[i].classList.contains('MaQrzd')){
				elements[i].insertBefore(element, elements[i].firstChild);
			}
		}
	}
};
const setup = () => {
	let style = `
.nF6pT.yqoUIf .FMTudf {
    white-space: normal !important;
}
`;
	document.querySelector(`head`).insertAdjacentHTML('beforeend', `<style id="custom_css"></style>`);
	document.querySelector(`#custom_css`).innerHTML = style;
	
	const observer = new MutationObserver( (records) => {
		replyRender();
	});
	observer.observe(document.body,{ childList: true, subtree: true });

	replyRender();
};
/* wait for the end of the document loading process before running setup */
window.onload = () => setup();
})();


	